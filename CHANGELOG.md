# Julho 2023

- Corrige a cor da fonte dos botões de compartilhamento
- Adiciona Google Analytics nas configurações do site
- corrige imagem que renderiza ao compartilhar nas redes sociais e adiciona "Foto de capa" nas configurações do site (open graph)

# Junho 2023

- Corrige erro de invalid signature quando a sessão não existe ou foi expirada
- Mantém a barra de navegação superior da home igual está nas outras páginas para melhorar a acessibilidade do menu
- Corrige favicon dinâmico
- Corrige ícone do menu na versão mobile
- Corrige console erro de mutação de propriedade indevida no componente de seleção de cor
- Adiciona configuração de fontes nas configurações principais do site e nas configurações de cada seção
- Aumenta o tamanho máximo da logo no cabeçalho

# RELEASE - Biblioteca (31/01/2022)


Bom dia pessoal!
Finalizamos a publicação da nova biblioteca com os dados importados do Mendeley.
Nos últimos dias fizemos várias adaptações e melhorias na biblioteca, dentre as principais: 

Adicionamos novos campos nos cadastros e na visualização da postagem:

  - Tipo de documento
  - Autores
  - Cidade
  - Organizadores
  - Identificador de Objeto Digital
  - Instituição
  - Número da publicação
  - Idiomas
  - Anotações
  - Páginas
  - Disponibilidade
  - Fonte
  - Volume
  - Informações adicionais

Adicionados novos filtros na biblioteca. Agora temos:
  - Busca (título, tags)
  - Categoria
  - Tipo de publicação
  - Idioma
  (Esses filtros são somatórios)

Adicionamos a paginação no painel e na busca da biblioteca (30 itens por página)
F
Além disso adicionamos várias melhorias visuais e de performance

Por enquanto não tem link no site para a biblioteca (assim que aprovadas essas alterações nós adicionaremos) mas a biblioteca já pode ser acessada:

https://www.observatoriodacastanha.org.br/biblioteca

https://www.observatoriodacastanha.org.br/conta/medias

Sugestão de melhorias para próximas fases:

Definir quais são os campos do formulário são relacionados com cada tipo de publicação. 

Exemplo: Se a pessoa selecionar o tipo de publicação "Notícia" quais os campos de formulário devem aparecer abaixo?



