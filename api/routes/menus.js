const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()
const auth = require('../config/auth')
const Menu = mongoose.model('Menu')

router.get('/', async (req, res) => {
  try {
    const menus = await Menu.find({ menu: null })
      .populate({
        path: 'submenus',
        populate: {
          path: 'page',
          select: 'slug'
        }
      })
      .populate({
        path: 'page',
        select: 'slug'
      })
      .sort('order')
    res.json(menus)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

function setMenuItem(menu) {
  return {
    _id: menu._id.toString(),
    name: menu.name,
    page: menu.page,
    url: menu.url || ''
  }
}

router.get('/submenus', async (req, res) => {
  try {
    // const menus = (await Menu.find({ menu: null }).populate('submenu').sort('name')).map(menu => {
    //   let menuItem = setMenuItem(menu)
    //   menuItem.submenus = menuItem.submenu ? [menuItem.submenu] : []
    //   return menuItem
    // })

    const menus = (await Menu.find({ menu: null })
      .populate(req.query.populate)
      .sort('name'))
      .map(menu => setMenuItem(menu))

    for (let i = 0; i < menus.length; i++) {
      const submenus = (await Menu.find({ menu: menus[i]._id })
        .populate(req.query.populate)
        .sort('name'))
        .map(menu => setMenuItem(menu))

      if (submenus) {
        menus[i].submenus = submenus
      }
    }

    res.json(menus)
  } catch (err) {
    res.status(422).json(err.message)
  }
})

router.get('/:id', async (req, res) => {
  try {
    const menu = await Menu.findOne({ _id: req.params.id })
    res.json(menu)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.post('/', auth.admin, async (req, res) => {
  const newMenu = new Menu(req.body)
  try {
    await newMenu.save()
    res.send(newMenu)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.put('/reorder', auth.admin, async (req, res) => {
  if (req.body.menus) {
    for (const item of req.body.menus) {
      await Menu.findOneAndUpdate(
        {
          _id: item.id
        },
        {
          $set: { order: item.order, menu: item.menu }
        },
        {
          upsert: true
        }
      )
    }
  }
  res.json('ok')
})

router.put('/:id', auth.admin, async (req, res) => {
  const params = req.body

  try {
    const menu = await Menu.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: params
    }, {
      upsert: true
    })

    res.json(menu)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.delete('/:id', auth.admin, async (req, res) => {
  try {
    const menu = await Menu.findByIdAndDelete(req.params.id)
    res.send(menu)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

module.exports = router
