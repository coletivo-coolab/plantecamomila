const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()
const auth = require('../config/auth')
const Settings = mongoose.model('Settings')
const User = mongoose.model('User')

router.get('/', async (req, res) => {
  try {
    const settings = await Settings.findOne()
    res.json(settings)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.post('/', auth.admin, async (req, res) => {
  const settings = await Settings.findOne()
  Object.keys(req.body).forEach(key => {
    settings[key] = req.body[key]
  })

  try {
    await settings.save()
    res.send(settings)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.post('/setup', async (req, res) => {
  let settings = await Settings.findOne()
  if (!settings) {
    settings = new Settings({ title: req.body.title })
    await settings.save()
    const admin = new User({
      name: req.body.admin_name,
      email: req.body.email,
      role: 'super'
    })
    admin.setPassword(req.body.password)
    await admin.save()
    res.send({ settings, admin: admin.data() })
  }
})

module.exports = router
