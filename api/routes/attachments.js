const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()
const auth = require('../config/auth')
const Attachment = mongoose.model('Attachment')

router.get('/', async (req, res) => {
  const query = {
  }
  if (req.query.type) {
    query.type = req.query.type
  }

  try {
    const attachments = await Attachment.find(query).populate(req.query.populate).sort({ createdAt: -1 })
    res.send(attachments)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.post('/', auth.admin, async (req, res) => {
  const attachment = new Attachment(req.body)
  await attachment.save()
  res.send(attachment)
})

router.get('/:id', async (req, res) => {
  try {
    const attachment = await Attachment.findOne({
      slug: req.params.id
    })
    res.send(attachment)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.put('/:id/duplicate', async (req, res) => {
  const attachment = await Attachment.findById(req.params.id)
  const duplicated = new Attachment({
    type: attachment.type,
    url: attachment.url,
    thumb: attachment.thumb,
    average: attachment.average,
    title: attachment.title,
    description: attachment.description,
    credits: attachment.credits,
    link: attachment.link,
    link_title: attachment.link_title
  })
  await duplicated.save()
  res.send(duplicated)
})

router.put('/:id', auth.admin, async (req, res) => {
  const params = req.body
  const attachment = await Attachment.findOne({ _id: req.params.id })
  Object.keys(params).forEach(key => {
    attachment[key] = params[key]
  })
  await attachment.save().then(async attachment => {
    res.send(await Attachment.findById(attachment._id))
  }).catch(err => {
    res.status(422).send(err.message)
  })
})

router.put('/counter/:id', async (req, res) => {
  const filter = { _id: req.params.id }
  const attachment = await Attachment.findOne(filter)
  try {
    const doc = await Attachment.findOneAndUpdate(
      filter,
      {
        $set: { counter: (attachment.counter + 1) }
      },
      {
        upsert: true
      }
    )

    res.send(doc)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

module.exports = router
