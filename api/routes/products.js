const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()
const slugify = require('slugify')
const { admin } = require('../config/auth')
const Product = mongoose.model('Product')

router.get('/', admin, async (req, res) => {
  const query = { deleted: { $ne: true } }
  if (req.query.search) {
    query.name = { $regex: req.query.search, $options: 'i' }
  }

  try {
    const products = await Product.find(query).populate('orders')
    res.send(products)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/current_tags', async (req, res) => {
  const query = { deleted: { $ne: true } }

  try {
    const products = await Product.find(query).select('tags')
    const tags = {}
    products.forEach(product => {
      if (product.tags) {
        product.tags.forEach(tag => {
          tags[tag] = true
        })
      }
    })
    res.json(Object.keys(tags).sort((a, b) => a.localeCompare(b)))
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/:id', admin, async (req, res) => {
  try {
    const product = await Product.findOne({
      $or: [
        { _id: req.params.id }, { slug: req.params.id }
      ]
    })

    res.json(product)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.post('/', admin, async (req, res) => {
  const newProduct = new Product(req.body)
  newProduct.slug = slugify(newProduct.name).toLowerCase()
  try {
    await newProduct.save()
    res.send(newProduct)
  } catch (error) {
    res.status(422).send(error.message)
  }
})

router.put('/:id', admin, async (req, res) => {
  const params = req.body
  try {
    const product = await Product.findOneAndUpdate({
      $or: [
        { _id: req.params.id }, { slug: req.params.id }
      ]
    }, {
      $set: params
    }, {
      upsert: true
    })

    res.send(product)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.delete('/:id', admin, async (req, res) => {
  try {
    const product = await Product.findOne({
      $or: [
        { _id: req.params.id }, { slug: req.params.id }
      ]
    })

    product.deleted = true
    product.save()
    res.send(product)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

module.exports = router
