import fs from 'fs'
import { S3, PutObjectCommand } from '@aws-sdk/client-s3'

const s3Config = {
  endpoint: process.env.SPACES_ENDPOINT,
  defaultBucket: process.env.SPACES_BUCKET,
  region: 'us-east-1',
  credentials: {
    accessKeyId: process.env.SPACES_KEY,
    secretAccessKey: process.env.SPACES_SECRET
  }
}

export const uploadFileToS3 = async ({
  filename,
  originalFilePath,
  targetFolder,
  fileBuffer,
  mimeType
}) => {
  if (s3Config.endpoint) {
    const s3Client = new S3(s3Config)

    // get file from path
    const fileData = fileBuffer || fs.readFileSync(originalFilePath)

    const params = {
      Bucket: s3Config.defaultBucket,
      Key: process.env.APP_NAME + '/' + targetFolder + filename,
      Body: fileData,
      ACL: 'public-read'
    }

    if (mimeType) {
      params.ContentType = mimeType
    }

    try {
      const requestData = await s3Client.send(new PutObjectCommand(params))
      return requestData
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error)
    }
  }
}
