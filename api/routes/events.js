const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()
const auth = require('../config/auth')
const Event = mongoose.model('Event')

router.get('/', async (req, res) => {
  try {
    const events = await Event.find({}).populate(req.query.populate).sort({ start_at: -1 })
    res.send(events)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/current_tags', async (req, res) => {
  try {
    const medias = await Event.find().select('tags')
    const tags = {}
    medias.forEach(media => {
      if (media.tags) {
        media.tags.forEach(tag => {
          tags[tag] = true
        })
      }
    })
    res.json(Object.keys(tags).sort((a, b) => a.localeCompare(b)))
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/:id', async (req, res) => {
  try {
    const event = await Event.findOne({
      _id: req.params.id
    })
    res.json(event)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.post('/', auth.admin, async (req, res) => {
  const newEvent = new Event(req.body)
  await newEvent.save()
  res.send(newEvent)
})

router.put('/:id', auth.admin, async (req, res) => {
  const params = req.body
  try {
    const event = await Event.findOneAndUpdate({
      _id: req.params.id
    }, {
      $set: params
    }, {
      upsert: true
    })
    res.json(event)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.delete('/:id', auth.admin, async (req, res) => {
  try {
    const event = await Event.findByIdAndDelete(req.params.id)

    res.json(event)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

module.exports = router
