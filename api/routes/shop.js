const { calcularPrecoPrazo } = require('correios-brasil')
const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()
const auth = require('../config/auth')
const Product = mongoose.model('Product')
const Order = mongoose.model('Order')
const { optionText } = require('../../utils')

router.get('/products', async (req, res) => {
  const query = { deleted: { $ne: true }, published: true, qtd: { $gt: 0 } }
  if (req.query.search) {
    query.name = { $regex: req.query.search, $options: 'i' }
  }
  if (req.query.tag) {
    query.tags = req.query.tag
  }

  try {
    let products = await Product.find(query).populate('orders')
    switch (req.query.sort) {
      case 'most_recent':
        products = products.sort((a, b) => {
          return new Date(b.createdAt) - new Date(a.createdAt)
        })
        break
      case 'best_sellers':
        products = products.sort((a, b) => {
          return b.qtd_ordered - a.qtd_ordered
        })
        break
      case 'alphabetical_order':
        products = products.sort((a, b) => {
          return a.name.localeCompare(b.name)
        })
        break
    }
    res.json(products)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/tags', async (req, res) => {
  const query = { deleted: { $ne: true }, published: true, qtd: { $gt: 0 } }

  try {
    const products = await Product.find(query, 'tags')
    const tags = {}
    products.forEach(product => {
      if (product && product.tags) {
        product.tags.forEach(tag => {
          tags[tag] = true
        })
      }
    })
    res.json(Object.keys(tags).sort((a, b) => {
      return a.localeCompare(b)
    }))
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/product/:id', async (req, res) => {
  try {
    const product = await Product.findOne({
      $or: [
        { _id: req.params.id }, { slug: req.params.id }
      ]
    })
    res.json(product)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/related/:id', async (req, res) => {
  try {
    const product = await Product.findOne({
      $or: [
        { _id: req.params.id }, { slug: req.params.id }
      ]
    })
    let relatedProducts = []
    if (product.tags) {
      relatedProducts = await Product.find({ tags: { $in: product.tags }, deleted: { $ne: true }, published: true, qtd: { $gt: 0 }, _id: { $ne: req.params.id } })
    }
    res.json(relatedProducts)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.post('/order', auth.authenticated, async (req, res) => {
  try {
    const latest = await Order.find().sort({ code: -1 }).limit(1)
    const newOrder = new Order(req.body)

    newOrder.status = 'pending'
    newOrder.user = req.user._id

    if (latest && latest.length) {
      newOrder.code = latest[0].code + 1
    } else {
      newOrder.code = 1
    }

    req.body.cart.forEach(item => {
      newOrder.items.push({
        product: item.product._id,
        qtd: item.qtd,
        price: item.product.price,
        total: item.qtd * item.product.price
      })
    })

    await newOrder.save()
    res.send(newOrder)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.post('/calc_shipping', async (req, res) => {
  try {
    const product = await Product.findOne({
      $or: [
        { _id: req.body.product }, { slug: req.body.product }
      ]
    })

    const args = {
      sCepOrigem: req.body.destination,
      sCepDestino: req.body.source,
      nVlPeso: product.weight,
      nCdFormato: product.format,
      nVlComprimento: product.length,
      nVlAltura: product.height,
      nVlLargura: product.width,
      nCdServico: product.shipping_services.filter(service => service),
      nVlDiametro: product.diameter
    }
    let shipping = await calcularPrecoPrazo(args).catch((e) => {
      // eslint-disable-next-line no-console
      console.log(e)
    })
    // eslint-disable-next-line no-console
    console.log(shipping)
    if (shipping && shipping.length) {
      shipping = shipping
        .filter(item => item.Erro === '0')
        .map(option => {
          return {
            price: Number(option.Valor.replace('.', '').replace(',', '.')),
            code: option.Codigo,
            description: optionText(option.Codigo, 'servicos-correios'),
            delivery_time: option.PrazoEntrega,
            delivery_saturday: option.EntregaSabado === 'S'
          }
        }).sort((a, b) => {
          return a.price - b.price
        })
      res.send(shipping)
    } else {
      res.status(422).send('Não foi possível calcular o frete')
    }
  } catch (e) {
    res.status(422).send(e.message)
  }
})
module.exports = router
