const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()
const slugify = require('slugify')
const auth = require('../config/auth')
const Page = mongoose.model('Page')

router.get('/', async (req, res) => {
  try {
    const pages = await Page.find({}).populate(req.query.populate)
    res.json(pages)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/current_tags', async (req, res) => {
  try {
    const pages = await Page.find().select('tags')
    const tags = {}
    pages.forEach(page => {
      if (page.tags) {
        page.tags.forEach(tag => {
          tags[tag] = true
        })
      }
    })
    res.json(Object.keys(tags).sort((a, b) => a.localeCompare(b)))
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/:id', async (req, res) => {
  try {
    const page = await Page.findOne({
      slug: req.params.id
    })
    res.json(page)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.post('/', auth.admin, async (req, res) => {
  const newPage = new Page(req.body)
  if (!newPage.slug) {
    newPage.slug = slugify(newPage.title).toLowerCase()
  }

  try {
    await newPage.save()
    res.send(newPage)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.put('/:id', auth.admin, async (req, res) => {
  const params = req.body
  const page = await Page.findOne({ _id: req.params.id })
  Object.keys(params).forEach(key => {
    page[key] = params[key]
  })
  await page.save().then(page => {
    res.send(page)
  }).catch(err => {
    res.status(422).send(err.message)
  })
})

router.delete('/:id', auth.admin, async (req, res) => {
  try {
    const page = await Page.findByIdAndDelete(req.params.id)

    res.send(page)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

module.exports = router
