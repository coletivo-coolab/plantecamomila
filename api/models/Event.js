const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId
const { downloadBase64 } = require('./utils')

const EventSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: String,
  content: String,
  image: {
    type: ObjectId,
    ref: 'Attachment',
    autopopulate: true
  },
  docs: [{
    type: ObjectId,
    ref: 'Attachment',
    autopopulate: true
  }],
  tags: [String],
  start_at: Date,
  end_at: Date
}, {
  timestamps: true,
  toJSON: { virtuals: true }
})

EventSchema.plugin(require('mongoose-autopopulate'))

EventSchema.pre('save', function() {
  this.content = downloadBase64(this.content, this.slug)
})

const Event = mongoose.models.Event || mongoose.model('Event', EventSchema)
module.exports = Event
