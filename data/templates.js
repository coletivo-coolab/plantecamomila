module.exports = [
  { /* Template 1 */
    text: 'Template 1',
    value: [{ background: null, background_fluid: null, attrs: { fluid: false }, columns: [{ background: null, attrs: { lg: '6' }, components: [{ type: 'banners', attrs: { class: '', items: [{ _id: '6288189be7092b1a9a3af7e8', url: 'https://picsum.photos/300/200', type: 'documents', __v: 0 }] }, content: null, category: 'banners' }] }, { background: null, attrs: { lg: '6' }, components: [{ type: 'h1', attrs: { class: '' }, content: 'Lorem ipsum', category: 'title' }, { type: 'div', attrs: { class: '' }, content: '<p><span style="color: rgb(0, 0, 0);">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p><p><br></p><p><strong style="color: rgb(0, 0, 0);">Duis aute irure dolor in reprehenderit</strong></p>', category: 'paragraph' }] }] }, { background: null, background_fluid: null, attrs: { fluid: false }, columns: [{ background: null, attrs: { lg: '12' }, components: [{ type: 'div', attrs: { class: '' }, content: '<h3><strong>The standard Lorem Ipsum passage, used since the 1500s</strong></h3><p class="ql-align-justify">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p><h3><strong>Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</strong></h3><p class="ql-align-justify">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p><p><br></p>', category: 'paragraph' }] }] }, { background: null, background_fluid: null, attrs: { fluid: false }, columns: [{ background: null, attrs: { lg: '4' }, components: [{ type: 'card', attrs: { class: '', 'img-src': { url: 'https://picsum.photos/200' }, header: '', title: 'Sedquiano', 'sub-title': '', text: 'Nemo enim ipsam ' }, content: null, category: 'card' }] }, { background: null, attrs: { lg: '4' }, components: [{ type: 'card', attrs: { class: '', 'img-src': { url: 'https://picsum.photos/200?randon=1' }, text: 'reprehenderit qui in ea voluptate', header: '', title: 'Upidatata' }, content: null, category: 'card' }] }, { background: null, attrs: { lg: '4' }, components: [{ type: 'card', attrs: { class: '', 'img-src': { url: 'https://picsum.photos/200?randon=3' }, title: 'Adipisci velit', 'sub-title': '', text: 'sed quia non numquam' }, content: null, category: 'card' }] }] }]
  },
  { /* Template 2 */
    text: 'Template 2',
    value: [{ background: null, background_fluid: null, attrs: { fluid: false }, columns: [{ background: null, attrs: { lg: '6' }, components: [{ type: 'banners', attrs: { class: '', items: [{ _id: '6209a3b02e7cca34b06a14ac', type: 'images', url: '/api/uploads/images/pngtree-abstract-bg-image_914283.png', thumb: '/api/uploads/images/thumbs/pngtree-abstract-bg-image_914283.png', average: '/api/uploads/images/averages/pngtree-abstract-bg-image_914283.png', __v: 0, description: '', link: '', link_title: '', title: '' }] }, content: null, category: 'banners' }] }, { background: null, attrs: { lg: '6' }, components: [{ type: 'h1', attrs: { class: '' }, content: 'Lorem ipsum', category: 'title' }, { type: 'div', attrs: { class: '' }, content: '<p><span style="color: rgb(0, 0, 0);">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p>', category: 'paragraph' }] }] }]
  },
  { /* Template 3 */
    text: 'Template 3',
    value: [{ background: null, background_fluid: null, attrs: { fluid: false }, columns: [{ background: null, attrs: { lg: '12' }, components: [{ type: 'h5', attrs: { class: '' }, content: 'Teste de título', category: 'title' }, { type: 'banners', attrs: { class: '', items: [{ _id: '620ae10d41af80aaf366883a', type: 'images', url: '/api/uploads/images/LOGO.jpg', thumb: '/api/uploads/images/thumbs/LOGO.jpg', average: '/api/uploads/images/averages/LOGO.jpg', __v: 0 }] }, content: null, category: 'banners' }] }] }]
  },
  { /* Template 4 */
    text: 'Template 4',
    value: [
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: 'pt-0 pb-0'
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: 'p-0'
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      _id: '650476f057d416c8f7660297',
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      __v: 0,
                      credits: '',
                      description: '',
                      link: '',
                      link_title: '',
                      title: 'Titulo da imagem'
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#e0e0db',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#e0e0db',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#e0e0db',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<h2><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</span></h2>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#e0e0db',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#e0e0db',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: ''
                },
                content: 'Lorem Ipsum is simply dummy text.',
                category: 'title'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><br></p><p><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.&nbsp;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p><p><br></p><p><span style=\"color: rgb(0, 0, 0);\">﻿</span></p>",
                category: 'paragraph'
              },
              {
                type: 'b-button',
                attrs: {
                  class: '',
                  size: 'lg',
                  variant: 'primary',
                  outlined: false
                },
                content: 'Texto do botão',
                category: 'button'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '64ff062b57d416c8f765f262',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '64ff062b57d416c8f765f262',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: ''
                },
                content: 'Lorem Ipsum is simply dummy text.',
                category: 'title'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><br></p><p><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.&nbsp;It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p><p><br></p><p><span style=\"color: rgb(0, 0, 0);\">﻿</span></p>",
                category: 'paragraph'
              },
              {
                type: 'b-button',
                attrs: {
                  class: '',
                  size: 'lg',
                  variant: 'primary',
                  outlined: false
                },
                content: 'Texto do botão',
                category: 'button'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: 'pr-0 pl-0'
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      _id: '650476fb57d416c8f766029e',
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      __v: 0,
                      credits: '',
                      description: '',
                      link: '',
                      link_title: '',
                      title: 'Titulo da imagem'
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<h3><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</strong></h3>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><span style=\"color: rgb(0, 0, 0);\">Lorem Ipson&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></p>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          }
        ]
      }
    ]
  },
  { /* Template 5 */
    text: 'Template 5',
    value: [
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: 'pt-0 pb-0'
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: 'p-0'
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      _id: '650476b357d416c8f7660269',
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      __v: 0,
                      credits: '',
                      description: '',
                      link: '',
                      link_title: '',
                      title: 'Titulo da imagem'
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#38383b',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#38383b',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '10',
              'background-color': '#38383b',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: '',
                  color: '#ffffff'
                },
                content: ' Lorem Ipson',
                category: 'title'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#38383b',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#38383b',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#38383b',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '650476c757d416c8f7660273',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#38383b',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '650476cd57d416c8f7660277',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#38383b',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#38383b',
              class: ''
            },
            components: [
              {
                type: null,
                attrs: {
                  class: ''
                },
                content: null,
                category: null
              }
            ]
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#38383b',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<h4><strong style=\"color: rgb(226, 226, 226);\">Lorem Ipsum</strong><span style=\"color: rgb(226, 226, 226);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></h4>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '6',
              'background-color': '#38383b',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#38383b',
              class: ''
            },
            components: [
              {
                type: null,
                attrs: {
                  class: ''
                },
                content: null,
                category: null
              }
            ]
          },
          {
            attrs: {
              lg: '11',
              'background-color': '#38383b',
              class: ''
            },
            components: [
              {
                type: 'b-button',
                attrs: {
                  class: '',
                  size: 'lg',
                  variant: 'secondary',
                  outlined: false
                },
                content: 'Texto do botão',
                category: 'button'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: '<p><br></p><p><br></p>',
                category: 'paragraph'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#a8a6a1',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#a8a6a1',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#a8a6a1',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<h3><br></h3><h3><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500</span></h3>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#a8a6a1',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#a8a6a1',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><br></p><p><span style=\"color: rgb(0, 0, 0);\">Is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500</span></p><p><br></p><p><span style=\"color: rgb(0, 0, 0);\">﻿</span></p>",
                category: 'paragraph'
              },
              {
                type: 'b-button',
                attrs: {
                  class: '',
                  size: 'lg',
                  variant: 'primary',
                  outlined: false
                },
                content: 'Texto do botão',
                category: 'button'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#a8a6a1',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: 'pt-0 pb-0'
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: 'pr-0 pl-0 pt-0'
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '650476e157d416c8f766027d',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          }
        ]
      }
    ]
  },
  { /* Template 6 */
    text: 'Template 6',
    value: [
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: 'pt-0 pb-0'
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: 'p-0'
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504771357d416c8f76602bb',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#bfb1c4',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '2',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504771c57d416c8f76602c1',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: ''
                },
                content: 'Lorem Ipson',
                category: 'title'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><br></p><h4><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></h4><p><br></p>",
                category: 'paragraph'
              },
              {
                type: 'b-button',
                attrs: {
                  class: '',
                  size: 'lg',
                  variant: 'primary',
                  outlined: false
                },
                content: 'Texto do botão',
                category: 'button'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#bfb1c4',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '2',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: ''
                },
                content: 'Lorem Ipson',
                category: 'title'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><br></p><h4><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></h4><p><br></p>",
                category: 'paragraph'
              },
              {
                type: 'b-button',
                attrs: {
                  class: '',
                  size: 'lg',
                  variant: 'primary',
                  outlined: false
                },
                content: 'Texto do botão',
                category: 'button'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504772157d416c8f76602c5',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#ebeaeb',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '12',
              'background-color': '#ebeaeb',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: '',
                  align: 'center'
                },
                content: 'Logos',
                category: 'title'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#ebeaeb',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#ebeaeb',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: '/api/uploads/images/Logo1.png',
                      thumb: '/api/uploads/images/thumbs/Logo1.png',
                      average: '/api/uploads/images/averages/Logo1.png',
                      counter: 0,
                      _id: '64ff0d9d57d416c8f765f320',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#ebeaeb',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: '/api/uploads/images/Logo3.png',
                      thumb: '/api/uploads/images/thumbs/Logo3.png',
                      average: '/api/uploads/images/averages/Logo3.png',
                      counter: 0,
                      _id: '64ff0da657d416c8f765f323',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#ebeaeb',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: '/api/uploads/images/Logo1.png',
                      thumb: '/api/uploads/images/thumbs/Logo1.png',
                      average: '/api/uploads/images/averages/Logo1.png',
                      counter: 0,
                      _id: '64ff0deb57d416c8f765f331',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#ebeaeb',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: '/api/uploads/images/Logo3.png',
                      thumb: '/api/uploads/images/thumbs/Logo3.png',
                      average: '/api/uploads/images/averages/Logo3.png',
                      counter: 0,
                      _id: '64ff0db257d416c8f765f32b',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#ebeaeb',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: '/api/uploads/images/Logo1.png',
                      thumb: '/api/uploads/images/thumbs/Logo1.png',
                      average: '/api/uploads/images/averages/Logo1.png',
                      counter: 0,
                      _id: '64ff0dac57d416c8f765f327',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#ebeaeb',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: null,
                attrs: {
                  class: ''
                },
                content: null,
                category: null
              }
            ]
          },
          {
            attrs: {
              lg: '10',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<h3 class=\"ql-align-center\"><strong style=\"color: rgb(0, 0, 0);\">\"Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.\"</span></h3><p class=\"ql-align-center\"><br></p><p class=\"ql-align-center\">- Ipson, L</p>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: 'pt-0 pb-0'
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: 'p-0'
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504772b57d416c8f76602c9',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          }
        ]
      }
    ]
  },
  { /* Template 7 */
    text: 'Template 7',
    value: [
      {
        attrs: {
          fluid: true,
          'background-color': '#fdf0e6',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '6',
              'background-color': '#fdf0e6',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504774e57d416c8f76602e3',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fdf0e6',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fdf0e6',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: '<p><br></p><p><br></p><p><br></p><p><br></p>',
                category: 'paragraph'
              },
              {
                type: 'h1',
                attrs: {
                  class: ''
                },
                content: 'Lorem Ipsum',
                category: 'title'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<h3><br></h3><h2><br></h2><h1><strong style=\"color: rgb(0, 0, 0);\">﻿Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></h1>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fdf0e6',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: '',
                  align: 'center',
                  color: '#f2b0a5'
                },
                content: 'Lorem Ipson',
                category: 'title'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '8',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p class=\"ql-align-center\"><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</span></p>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#fff',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '4',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504777757d416c8f76602ef',
                    __v: 0
                  },
                  title: 'Lorem Ipson',
                  align: 'center',
                  'sub-title': '',
                  text: "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ",
                  'background-color': '#ffffff',
                  'title-color': '#ff8906',
                  'text-color': '#212529',
                  links: [
                    {
                      title: 'Saiba Mais',
                      to: '/teste'
                    }
                  ]
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504777757d416c8f76602ef',
                    __v: 0
                  },
                  title: 'Lorem Ipson',
                  align: 'center',
                  'sub-title': '',
                  text: "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ",
                  'background-color': '#ffffff',
                  'title-color': '#ff8906',
                  'text-color': '#212529',
                  links: [
                    {
                      title: 'Saiba Mais',
                      to: '/teste'
                    }
                  ]
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504777757d416c8f76602ef',
                    __v: 0
                  },
                  title: 'Lorem Ipson',
                  align: 'center',
                  'sub-title': '',
                  text: "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ",
                  'background-color': '#ffffff',
                  'title-color': '#ff8906',
                  'text-color': '#212529',
                  links: [
                    {
                      title: 'Saiba Mais',
                      to: '/teste'
                    }
                  ]
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504777757d416c8f76602ef',
                    __v: 0
                  },
                  title: 'Lorem Ipson',
                  align: 'center',
                  'sub-title': '',
                  text: "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ",
                  'background-color': '#ffffff',
                  'title-color': '#ff8906',
                  'text-color': '#212529',
                  links: [
                    {
                      title: 'Saiba Mais',
                      to: '/teste'
                    }
                  ]
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504777757d416c8f76602ef',
                    __v: 0
                  },
                  title: 'Lorem Ipson',
                  align: 'center',
                  'sub-title': '',
                  text: "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ",
                  'background-color': '#ffffff',
                  'title-color': '#ff8906',
                  'text-color': '#212529',
                  links: [
                    {
                      title: 'Saiba Mais',
                      to: '/teste'
                    }
                  ]
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504777757d416c8f76602ef',
                    __v: 0
                  },
                  title: 'Lorem Ipson',
                  align: 'center',
                  'sub-title': '',
                  text: "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ",
                  'background-color': '#ffffff',
                  'title-color': '#ff8906',
                  'text-color': '#212529',
                  links: [
                    {
                      title: 'Saiba Mais',
                      to: '/teste'
                    }
                  ]
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504777757d416c8f76602ef',
                    __v: 0
                  },
                  title: 'Lorem Ipson',
                  align: 'center',
                  'sub-title': '',
                  text: "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ",
                  'background-color': '#ffffff',
                  'title-color': '#ff8906',
                  'text-color': '#212529',
                  links: [
                    {
                      title: 'Saiba Mais',
                      to: '/teste'
                    }
                  ]
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504777757d416c8f76602ef',
                    __v: 0
                  },
                  title: 'Lorem Ipson',
                  align: 'center',
                  'sub-title': '',
                  text: "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ",
                  'background-color': '#ffffff',
                  'title-color': '#ff8906',
                  'text-color': '#212529',
                  links: [
                    {
                      title: 'Saiba Mais',
                      to: '/teste'
                    }
                  ]
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504777757d416c8f76602ef',
                    __v: 0
                  },
                  title: 'Lorem Ipson',
                  align: 'center',
                  'sub-title': '',
                  text: "is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ",
                  'background-color': '#ffffff',
                  'title-color': '#ff8906',
                  'text-color': '#212529',
                  links: [
                    {
                      title: 'Saiba Mais',
                      to: '/teste'
                    }
                  ]
                },
                content: null,
                category: 'card'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fdf0e6',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#fdf0e6',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#fdf0e6',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504776457d416c8f76602eb',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fdf0e6',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#fdf0e6',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: ''
                },
                content: 'Lorem Ipson',
                category: 'title'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<h3><br></h3><h3><br></h3><h3><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></h3><p><br></p>",
                category: 'paragraph'
              },
              {
                type: 'b-button',
                attrs: {
                  class: '',
                  size: 'lg',
                  variant: 'primary',
                  outlined: false
                },
                content: 'Texto do botão',
                category: 'button'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fdf0e6',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fdf0e6',
          'background-fluid': false,
          class: 'pt-0 pb-0'
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fdf0e6',
              class: 'p-0'
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504775957d416c8f76602e7',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          }
        ]
      }
    ]
  },
  { /* Template 8 */
    text: 'Template 8',
    value: [
      {
        attrs: {
          fluid: true,
          'background-color': '#523d38',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#523d38',
              class: ''
            },
            components: [
              {
                type: 'h5',
                attrs: {
                  class: '',
                  color: '#ffffff',
                  align: 'center'
                },
                content: 'QUEM SOMOS',
                category: 'title'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: false,
          'background-color': '#fff',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '5',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504779057d416c8f7660309',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<h4><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</span></h4>",
                category: 'paragraph'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: '<p><br></p><p><em style="color: rgb(0, 0, 0);">when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</em></p>',
                category: 'paragraph'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#fff',
          'background-fluid': false,
          class: 'pr-0 pl-0'
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: 'p-0'
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504779657d416c8f766030d',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          }
        ]
      }
    ]
  },
  { /* Template 9 */
    text: 'Template 9',
    value: [
      {
        attrs: {
          fluid: false,
          'background-color': '#fff',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: '',
                  align: 'center'
                },
                content: 'Quem Somos',
                category: 'title'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '10',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '650477a357d416c8f7660327',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<h4 class=\"ql-align-center\"><strong style=\"color: rgb(0, 0, 0);\">Lorem Ipsum</strong><span style=\"color: rgb(0, 0, 0);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</span></h4>",
                category: 'paragraph'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: '<p><br></p><h5 class="ql-align-center"><span style="color: rgb(0, 0, 0);">&nbsp;It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing</span></h5>',
                category: 'paragraph'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#bfb1c4',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '10',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: '<p class="ql-align-center">L. Ipson</p><p class="ql-align-center"><br></p><h4 class="ql-align-center"><span style="color: rgb(0, 0, 0);">"It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."</span></h4><p class="ql-align-center"><br></p><p class="ql-align-center"><br></p>',
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#bfb1c4',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: false,
          'background-color': '#fff',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: '',
                  align: 'center'
                },
                content: 'Lorem Ipson',
                category: 'title'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: '<p class="ql-align-center"><br></p><p class="ql-align-center"><br></p><h4 class="ql-align-center">Lorem Ipson 1</h4><h4 class="ql-align-center">Lorem Ipson 2</h4><h4 class="ql-align-center">Lorem Ipson 3</h4><h4 class="ql-align-center">Lorem Ipson 4</h4><h4 class="ql-align-center">Lorem Ipson 5</h4>',
                category: 'paragraph'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: false,
          'background-color': '#fff',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '650477a957d416c8f766032b',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: false,
          'background-color': '#fff',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: '',
                  align: 'center'
                },
                content: 'Lorem Ipson',
                category: 'title'
              }
            ]
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '6',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p class=\"ql-align-center\"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p><p><br></p>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#fff',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#fff',
              class: ''
            },
            components: [
              {
                type: 'b-button',
                attrs: {
                  class: '',
                  size: 'lg',
                  variant: 'primary',
                  outlined: false
                },
                content: 'Texto do botão',
                category: 'button'
              }
            ]
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#fff',
              class: ''
            },
            components: []
          }
        ]
      }
    ]
  },
  { /* Template 10 */
    text: 'Template 10',
    value: [
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '650477ba57d416c8f7660345',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: '<p><br></p><p><br></p><p><br></p>',
                category: 'paragraph'
              },
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '650477c557d416c8f766034b',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: '<p><span style="color: rgb(226, 226, 226);">______ LOREM IPSON</span></p>',
                category: 'paragraph'
              },
              {
                type: 'h1',
                attrs: {
                  class: '',
                  align: 'left',
                  color: '#ffffff'
                },
                content: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.',
                category: 'title'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><br></p><p><strong style=\"color: rgb(226, 226, 226);\">&nbsp;Ipsum</strong><span style=\"color: rgb(226, 226, 226);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</span></p><p><br></p><p><strong style=\"color: rgb(226, 226, 226);\">Ipsum</strong><span style=\"color: rgb(226, 226, 226);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</span></p><p><br></p><p><strong style=\"color: rgb(226, 226, 226);\">&nbsp;Ipsum</strong><span style=\"color: rgb(226, 226, 226);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</span></p>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      }
    ]
  },
  { /* Template 11 */
    text: 'Template 11',
    value: [
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: 'pt-0 pb-0'
        },
        columns: [
          {
            attrs: {
              lg: 12,
              'background-color': '#212529',
              class: 'p-0'
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '650477da57d416c8f7660365',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: null,
                attrs: {
                  class: ''
                },
                content: null,
                category: null
              }
            ]
          },
          {
            attrs: {
              lg: '6',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '64ff4fce57d416c8f765f728',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '6',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: '',
                  color: '#ffffff',
                  align: 'center'
                },
                content: 'LOREM IPSON LOREM IPSON LOREM IPSON LOREM',
                category: 'title'
              }
            ]
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<h5><strong style=\"color: rgb(226, 226, 226);\">&nbsp;Ipsum</strong><span style=\"color: rgb(226, 226, 226);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</span></h5>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '4',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '650477e757d416c8f7660369',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '6',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: '<h1 class="ql-align-center"><span style="color: rgb(226, 226, 226);">~~~~ADICIONAR VIDEO ~~~~~~</span></h1>',
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '6',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: '',
                  align: 'center',
                  color: '#ffffff'
                },
                content: 'LOREM IPSON',
                category: 'title'
              }
            ]
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><strong style=\"color: rgb(226, 226, 226);\">Lorem Ipsum</strong><span style=\"color: rgb(226, 226, 226);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</span></p>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '6',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '64ff50f657d416c8f765f73f',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '6',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '64ff50f657d416c8f765f73f',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><strong style=\"color: rgb(226, 226, 226);\">Lorem Ipsum</strong><span style=\"color: rgb(226, 226, 226);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</span></p>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><strong style=\"color: rgb(226, 226, 226);\">Lorem Ipsum</strong><span style=\"color: rgb(226, 226, 226);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</span></p>",
                category: 'paragraph'
              }
            ]
          },
          {
            attrs: {
              lg: '6',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '64ff50f657d416c8f765f73f',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '64ff50f657d416c8f765f73f',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: '',
                  color: '#ffffff'
                },
                content: 'lorem Ipson',
                category: 'title'
              },
              {
                type: 'div',
                attrs: {
                  class: ''
                },
                content: "<p><br></p><p><strong style=\"color: rgb(226, 226, 226);\">Lorem Ipsum</strong><span style=\"color: rgb(226, 226, 226);\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</span></p><p><br></p><p><span style=\"color: rgb(226, 226, 226);\">﻿</span></p>",
                category: 'paragraph'
              },
              {
                type: 'b-button',
                attrs: {
                  class: '',
                  size: 'lg',
                  variant: 'secondary',
                  outlined: false
                },
                content: 'Texto do botão',
                category: 'button'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      }
    ]
  },
  { /* Template 12 */
    text: 'Template 12',
    value: [
      {
        attrs: {
          fluid: true,
          'background-color': '#e2e2e2',
          'background-fluid': true,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '2',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '8',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: '',
                  align: 'center'
                },
                content: "Rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                category: 'title'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#e2e2e2',
          'background-fluid': true,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780757d416c8f7660383',
                      __v: 0
                    },
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780857d416c8f7660386',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780d57d416c8f766038a',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#e2e2e2',
          'background-fluid': true,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780757d416c8f7660383',
                      __v: 0
                    },
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780857d416c8f7660386',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780d57d416c8f766038a',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#e2e2e2',
          'background-fluid': true,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780757d416c8f7660383',
                      __v: 0
                    },
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780857d416c8f7660386',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780d57d416c8f766038a',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#e2e2e2',
          'background-fluid': true,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '1',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780757d416c8f7660383',
                      __v: 0
                    },
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780857d416c8f7660386',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '5',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: [
              {
                type: 'banners',
                attrs: {
                  class: '',
                  items: [
                    {
                      type: 'images',
                      url: 'https://picsum.photos/1980/1024',
                      counter: 0,
                      _id: '6504780d57d416c8f766038a',
                      __v: 0
                    }
                  ]
                },
                content: null,
                category: 'banners'
              }
            ]
          },
          {
            attrs: {
              lg: '1',
              'background-color': '#e2e2e2',
              class: ''
            },
            components: []
          }
        ]
      }
    ]
  },
  { /* Template 13 */
    text: 'Template 13',
    value: [
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '8',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'h1',
                attrs: {
                  class: '',
                  align: 'center',
                  color: '#ffffff'
                },
                content: "rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                category: 'title'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504782957d416c8f76603a4',
                    __v: 0
                  },
                  'title-color': '#ffffff',
                  title: 'Lorem Ipson',
                  text: "rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                  'text-color': '#ffffff'
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504783257d416c8f76603ae',
                    __v: 0
                  },
                  'title-color': '#ffffff',
                  title: 'LOREM IPSON',
                  text: "rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                  'text-color': '#ffffff'
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504782957d416c8f76603a4',
                    __v: 0
                  },
                  'title-color': '#ffffff',
                  title: 'Lorem Ipson',
                  text: "rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                  'text-color': '#ffffff'
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504783257d416c8f76603ae',
                    __v: 0
                  },
                  'title-color': '#ffffff',
                  title: 'LOREM IPSON',
                  text: "rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                  'text-color': '#ffffff'
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504782957d416c8f76603a4',
                    __v: 0
                  },
                  'title-color': '#ffffff',
                  title: 'Lorem Ipson',
                  text: "rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                  'text-color': '#ffffff'
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504783257d416c8f76603ae',
                    __v: 0
                  },
                  'title-color': '#ffffff',
                  title: 'LOREM IPSON',
                  text: "rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                  'text-color': '#ffffff'
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      },
      {
        attrs: {
          fluid: true,
          'background-color': '#212529',
          'background-fluid': false,
          class: ''
        },
        columns: [
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504782957d416c8f76603a4',
                    __v: 0
                  },
                  'title-color': '#ffffff',
                  title: 'Lorem Ipson',
                  text: "rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                  'text-color': '#ffffff'
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          },
          {
            attrs: {
              lg: '3',
              'background-color': '#212529',
              class: ''
            },
            components: [
              {
                type: 'card',
                attrs: {
                  class: '',
                  orientation: 'vertical',
                  'img-src': {
                    type: 'images',
                    url: 'https://picsum.photos/1980/1024',
                    counter: 0,
                    _id: '6504783257d416c8f76603ae',
                    __v: 0
                  },
                  'title-color': '#ffffff',
                  title: 'LOREM IPSON',
                  text: "rem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard",
                  'text-color': '#ffffff'
                },
                content: null,
                category: 'card'
              }
            ]
          },
          {
            attrs: {
              lg: '2',
              'background-color': '#212529',
              class: ''
            },
            components: []
          }
        ]
      }
    ]
  }
]
