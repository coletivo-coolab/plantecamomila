export default {
  props: {
    data: {
      type: Object,
      default: () => {}
    }
  },
  computed: {
    filesURL() {
      return process.env.FILES_URL || ''
    }
  },
  methods: {
    fileURL(url) {
      if (url) {
        return url.startsWith('http') ? url : (this.filesURL || '') + url
      }
    }
  }
}
