import https from 'https'

export default function ({ $axios, $toast, $auth, app }) {
  $axios.defaults.httpsAgent = new https.Agent({ rejectUnauthorized: false })

  $axios.onError(error => {
    console.log(error)
    if ($toast) {
      if (error.response) {
        console.log(error.response)
        if (error.response.data) {
          console.log(error.response.data)
          if (error.response.status === 500 && error.response.data.error && error.response.data.error.message && error.response.data.error.message.includes('invalid signature')) {
            // $toast.error('Sessão expirada!')
            $auth.logout()
          } else if (error.response.data.message) {
            $toast.error(error.response.data.message)
          } else if (error.response.data.error) {
            $toast.error(error.response.data.error.message)
          } else {
            $toast.error(error.response.data)
          }
        } else {
          $toast.error(error.response)
        }
      }
    } else {
      // eslint-disable-next-line no-console
      console.log(error)
    }
    return false
  })
}
