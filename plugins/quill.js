import Vue from 'vue'
import VueQuillEditor from 'vue-quill-editor'
import Quill from 'quill'

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme
import colors from '../assets/css/colors.sass'

const Link = Quill.import('formats/link')
class CustomLink extends Link {
  static create(value) {
    const node = super.create(value)
    value = this.sanitize(value)
    node.setAttribute('href', value)
    if (!value.startsWith('http')) {
      node.removeAttribute('target')
    }
    return node
  }
}

Quill.register(CustomLink)

const options = [
  ['bold', 'italic', 'underline', 'strike'], // toggled buttons
  ['blockquote', 'code-block'],

  [{
    header: 1
  }, {
    header: 2
  }], // custom button values
  [{
    list: 'ordered'
  }, {
    list: 'bullet'
  }],
  [{
    script: 'sub'
  }, {
    script: 'super'
  }], // superscript/subscript
  [{
    indent: '-1'
  }, {
    indent: '+1'
  }], // outdent/indent
  [{
    direction: 'rtl'
  }], // text direction

  [{
    size: ['small', false, 'large', 'huge']
  }], // custom dropdown
  [{
    header: [1, 2, 3, 4, 5, 6, false]
  }],

  [{
    color: Object.values(colors)
  }, {
    background: []
  }], // dropdown with defaults from theme
  [{
    font: []
  }],
  [{
    align: []
  }],
  ['image', 'video', 'link', 'iframe'],
  ['clean'], // remove formatting button
  ['fullscreen']
]
Vue.use(VueQuillEditor, {
  modules: {
    toolbar: {
      container: options,
      handlers: {
        fullscreen() {
          const quillEditorTag = document.getElementsByClassName('quill-editor')[0]
          quillEditorTag.classList.toggle('fullscreen')
        },
        image: () => {
          document.getElementById('quillfile').click()
        },
        iframe() {
          const html = prompt('Insira o código para embedar:')
          const range = this.quill.getSelection()
          this.quill.clipboard.dangerouslyPasteHTML(range.index, html)
        }
      }
    }
  }
})
